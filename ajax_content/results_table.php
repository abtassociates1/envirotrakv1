<?php
// set up header to prevent IE from caching queries
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

$startTime = time();

$city = @$_GET['city'];
$state = @$_GET['state'];
$zip = @$_GET['zip'];
$sic = @$_GET['sic'];
$forceAll = (is_null(@$_GET['all'])) ? false : $_GET['all'];
$all = true;

// connect to the database and prepare the insert statement
// replace with your username, and password
$databaseName = 'zombie_watch';
$user = 'XXXXXXXXXX';
$password = 'XXXXXXXXXX';

try {
	$db = new PDO("mysql:dbname=$databaseName",  $user, $password);
}
catch (PDOException $e) {
	die("Error: {$e ->getMessage()}");
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$params = array();

// get the latest data year and month
$latestQuery = $db->prepare("SELECT Category, Value FROM zombie_watch.metadata where Category = 'Latest Year' OR Category = 'Latest Month'");
try {
	$latestQuery->execute();
	$results = $latestQuery->fetchAll(PDO::FETCH_ASSOC);
	foreach ($results as $row) {
		if ($row['Category'] == 'Latest Year') {
			$params['thisYear'] = $row['Value'];
			$summaryParams['thisYear'] = $row['Value'];
		}
		else if ($row['Category'] == 'Latest Month') {
			$params['thisMonth'] = $row['Value'];
			$summaryParams['thisMonth'] = $row['Value'];
		}
	}
}
catch (PDOException $e) {
	die("Error: {$e ->getMessage()}");
}

$results = null;
$latestQuery = null;

$whereArray = array('Year = :thisYear', 'Month = :thisMonth');
$summaryWhereArray = array('Year = :thisYear', 'Month = :thisMonth');
// parse the input values
// use preferential heirarchy: ZIP, city & state, SIC code
$zipSearch = false;
$citySearch = false;
$sicSearch = false;
if ($zip != null) {
	$zipSearch = true;

	// make sure that ZIP is 5 digits
	$zip = str_pad($zip, 5, '0', STR_PAD_LEFT);

	$whereArray[] = 'ZIP = :zip';
	$params['zip'] = $zip;
	$summaryWhereArray[] = 'ZIP = :zip';
	$summaryParams['zip'] = $zip;
}
else if ($city != null && $state != null) {
	$citySearch = true;

	$whereArray[] = 'City = :city';
	$whereArray[] = 'State = :state';
	$params['city'] = $city;
	$params['state'] = $state;
	$summaryWhereArray[] = 'City = :city';
	$summaryWhereArray[] = 'State = :state';
	$summaryParams['city'] = $city;
	$summaryParams['state'] = $state;
}
else if ($sic != null) {
	$sicSearch = true;

	$params['sic'] = $sic;
	$summaryWhereArray[] = 'SIC = :sic';
	$summaryParams['sic'] = $sic;
}
else {
	// throw an error since nothing was selected
}

$summaryWhere = implode(' AND ', $summaryWhereArray);

$summaryStart = time();

$summaryQuery = $db->prepare("
	SELECT
		facility_count,
		new_violations,
		new_inspections,
		new_enforcement_actions,
		violations_resolved
	FROM zombie_watch.summary
	WHERE $summaryWhere
");

try {
	$summaryQuery->execute($summaryParams);
	$results = $summaryQuery->fetchAll(PDO::FETCH_ASSOC);
}
catch (PDOException $e) {
	die("Error: {$e ->getMessage()}");
}
if (count($results) == 0) {
	// no results available for this query, so halt further queries and return a "zero records found" message
	$facilitiesCount = null;
	$new_violations = null;
	$new_inspections = null;
	$new_enforcement_actions = null;
	$violations_resolved = null;
}
else if (count($results) > 1) {
	// throw an error
}
else {
	$facilitiesCount = $results[0]['facility_count'];
	$new_violations = $results[0]['new_violations'];
	$new_inspections = $results[0]['new_inspections'];
	$new_enforcement_actions = $results[0]['new_enforcement_actions'];
	$violations_resolved = $results[0]['violations_resolved'];
}

$results = null;
$summaryQuery = null;

$cutoff = 1000;
if ($facilitiesCount > $cutoff && !$forceAll) {
	$all = false;
}


if (!$all) {
	$whereArray[] = '(new_violation = 1 OR new_inspection = 1 OR new_enforcement_action = 1 OR violation_resolved = 1)';
}

$where = implode(' AND ', $whereArray);

$queryStart = time();

if ($zipSearch || $citySearch) {
	$query = $db->prepare("
		SELECT
			a.Name,
			a.Address,
			a.City,
			a.State,
			a.ZIP,
			a.Compliance_report,
			a.Current_compliance_status,
			a.Formal_Enf_Action_5yrs,
			a.Days_since_last_inspection,
			a.new_violation,
			a.new_inspection,
			a.new_enforcement_action,
			a.violation_resolved,
			GROUP_CONCAT(c.Label SEPARATOR '<br />') AS SIC_Labels
		FROM (
			SELECT
				FRS,
				Name,
				Address,
				City,
				State,
				ZIP,
				Compliance_report,
				Current_compliance_status,
				Formal_Enf_Action_5yrs,
				Days_since_last_inspection,
				new_violation,
				new_inspection,
				new_enforcement_action,
				violation_resolved
			FROM zombie_watch.epa_echo_data
			WHERE $where
		) as a
		LEFT JOIN zombie_watch.facility_SIC_codes AS b ON (a.FRS = b.FRS)
		LEFT JOIN zombie_watch.SIC_code_name_lookup AS c ON (b.SIC = c.SIC)
		GROUP BY
			a.Name,
			a.Address,
			a.City,
			a.State,
			a.ZIP,
			a.Compliance_report,
			a.Current_compliance_status,
			a.Formal_Enf_Action_5yrs,
			a.Days_since_last_inspection,
			a.new_violation,
			a.new_inspection,
			a.new_enforcement_action,
			a.violation_resolved
	");
}
else {
	/*	SELECT all FRS in with the given SIC code first
	 *	LEFT JOIN to data
	 *	LEFT JOIN to get all SIC codes for each facility
	 *	LEFT JOIN to get names for SIC codes
	 *	in given time period
	 *
	 *	changing the order radically alters the performance of the query
	 *	doing a single join to the facility_SIC_codes table gives only the SIC code selected, leaving out all others
	 *	using IN or EXISTS clauses is much slower than this format
	 */
	$query = $db->prepare("
		SELECT
			b.Name,
			b.Address,
			b.City,
			b.State,
			b.ZIP,
			b.Compliance_report,
			b.Current_compliance_status,
			b.Formal_Enf_Action_5yrs,
			b.Days_since_last_inspection,
			b.new_violation,
			b.new_inspection,
			b.new_enforcement_action,
			b.violation_resolved,
			GROUP_CONCAT(d.Label SEPARATOR '<br />') AS SIC_Labels
		FROM (
			SELECT FRS FROM zombie_watch.facility_SIC_codes WHERE SIC = :sic
		) as a
		LEFT JOIN zombie_watch.epa_echo_data as b ON (a.FRS = b.FRS)
		LEFT JOIN zombie_watch.facility_SIC_codes AS c ON (b.FRS = c.FRS)
		LEFT JOIN zombie_watch.SIC_code_name_lookup AS d ON (c.SIC = d.SIC)
		WHERE $where
		GROUP BY
			b.Name,
			b.Address,
			b.City,
			b.State,
			b.ZIP,
			b.Compliance_report,
			b.Current_compliance_status,
			b.Formal_Enf_Action_5yrs,
			b.Days_since_last_inspection,
			b.new_violation,
			b.new_inspection,
			b.new_enforcement_action,
			b.violation_resolved
	");
}

try {
	$query->execute($params);
	$results = $query->fetchAll(PDO::FETCH_ASSOC);
}
catch (PDOException $e) {
	die("Error: {$e ->getMessage()}");
}

$labelStart = time();

if ($zip != null) {
	if (count($results) > 0) {
		$cityState = null;
		foreach($results as $row) {
			if (!is_null($row['City']) && !is_null($row['State'])) {
				$cityState = $row['City'] . ', ' . $row['State'];
				break;
			}
		}

		$regionLabel = 'ZIP Code ' . ((is_null($cityState)) ? $zip : "$zip ($cityState)");
	}
	else {
		// query for city/state
		$queryCity = $db->prepare('SELECT City, State FROM zombie_watch.epa_echo_data WHERE ZIP = :zip AND City IS NOT null AND State IS NOT null LIMIT 1');
		try {
			$queryCity->execute(array('zip' => $zip));
			$resultsCity = $queryCity->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (PDOException $e) {
			die("Error: {$e ->getMessage()}");
		}

		$regionLabel = 'ZIP Code ' . ((count($resultsCity) == 0) ? $zip : "$zip ({$resultsCity[0]['City']}, {$resultsCity[0]['State']})");
		$resultsCity = null;
		$queryCity = null;
	}
}
else if ($city != null && $state != null) {
	if (count($results) > 0) {
		$regionLabel = $results[0]['City'] . ', ' . $results[0]['State'];
	}
	else {
		// query for city/state
		$queryCity = $db->prepare('SELECT City, State FROM zombie_watch.epa_echo_data WHERE City = :city AND State = :state LIMIT 1');
		try {
			$queryCity->execute(array('city' => $city, 'state' => $state));
			$resultsCity = $queryCity->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (PDOException $e) {
			die("Error: {$e ->getMessage()}");
		}

		$regionLabel = (count($resultsCity) == 0) ? "$city, $state" : $resultsCity[0]['City'] . ', ' . $resultsCity[0]['State'];
		$resultsCity = null;
		$queryCity = null;
	}
}
else {
	if (count($results) > 0) {
		$regionLabel = (is_null($results[0]['SIC_Labels'])) ? "2-digit SIC code $sic" : $results[0]['SIC_Labels'];
	}
	else {
		$querySIC = $db->prepare('SELECT Label FROM zombie_watch.SIC_code_name_lookup WHERE SIC = :sic');
		try {
			$querySIC->execute(array('sic' => $sic));
			$resultsSIC = $querySIC->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (PDOException $e) {
			die("Error: {$e ->getMessage()}");
		}
		$regionLabel = (count($resultsSIC) == 0) ? "2-digit SIC code $sic" : $resultsSIC[0]['Label'];
		$resultsSIC = null;
		$querySIC = null;
	}
}

// calculate summary result
$summaryScore = $new_inspections + (2 * $violations_resolved) + (-2 * $new_enforcement_actions);
$positives = ($new_inspections > 0 || $violations_resolved > 0) ? true : false;
$negatives = ($new_violations > 0 || $new_enforcement_actions > 0) ? true : false;
foreach ($results as $row) {
	$summaryScore += ($row['new_violation'] == 1 && $row['Current_compliance_status'] == 'violation(s)') ? -2 : 0;
	$summaryScore += ($row['new_violation'] == 1 && $row['Current_compliance_status'] == 'serious violation(s)') ? -4 : 0;
}

if (!$positives && !$negatives) {
	$summaryScoreLabel = 'Not Much Going On';
	$summaryScoreClass = 'neutral_summary_cell';
}
else if ($summaryScore > 0 && !$negatives) {
	$summaryScoreLabel = 'Looking Good';
	$summaryScoreClass = 'good_summary_label';
}
else if ($summaryScore > 1 && $negatives) {
	$summaryScoreLabel = 'Moving In The Right Direction';
	$summaryScoreClass = 'good_summary_label';
}
else if (($summaryScore < 0 && !$positives) || $summaryScore < -1) {
	$summaryScoreLabel = 'Needs Improvement';
	$summaryScoreClass = 'bad_summary_label';

}
else {
	$summaryScoreLabel = '<span class="good_summary_label">Some Good</span><br /><span class="bad_summary_label">Some Bad</span>';
}

$endTime = time();
$totalDuration = $endTime - $startTime;
$summaryDuration = $queryStart - $summaryStart;
$queryDuration = $labelStart - $queryStart;
$labelDuration = $endTime - $labelStart;

$totalMinutes = str_pad((string) floor(($totalDuration) / 60), 2, '0', STR_PAD_LEFT);
$totalSeconds = str_pad((string) $totalDuration % 60, 2, '0', STR_PAD_LEFT);

$summaryMinutes = str_pad((string) floor(($summaryDuration) / 60), 2, '0', STR_PAD_LEFT);
$summarySeconds = str_pad((string) $summaryDuration % 60, 2, '0', STR_PAD_LEFT);

$queryMinutes = str_pad((string) floor(($queryDuration) / 60), 2, '0', STR_PAD_LEFT);
$querySeconds = str_pad((string) $queryDuration % 60, 2, '0', STR_PAD_LEFT);

$labelMinutes = str_pad((string) floor(($labelDuration) / 60), 2, '0', STR_PAD_LEFT);
$labelSeconds = str_pad((string) $labelDuration % 60, 2, '0', STR_PAD_LEFT);
?>
<!--Include FooTables-->
<link href="css/footable-0.1.css" rel="stylesheet" type="text/css" />
<script src="js/footable-0.1.js" type="text/javascript"></script>
<!--<script src="js/footable.sortable.js" type="text/javascript"></script>-->

<div class="row-fluid">
	<div class="span12 rightText" style="margin-bottom:10px">
		<a class="btn btn-primary " style="width:100px;float:left;line-height:2.4em" id="back_button">Back</a>
	</div>
	<div class="vertical-spacing">&nbsp;</div>
</div>

<script type="text/javascript">
	$(function() {
		$('table').footable();


		$(".footable-row-detail-inner div strong").css('color','red!important');





		$('#back_button').click(function() {
			$.ajax({
				cache: false,
				dataType: 'html',
				success: function (response) {
					$('#panel_content_holder').html(response);
				},
				url: 'ajax_content/run_query.html'
			});
		});

		$('#email_sign_up').click(function() {
			$('#inputEmail_container').removeClass('error');
			$('#inputEmail_container span.error').css('display','none');
			$('#inputEmailConfirmation_container').removeClass('error');
			$('#inputEmailConfirmation_container span.error').css('display','none');

			if (IsEmail($('#inputEmail').val())) {
				if ($('#inputEmail').val() == $('#inputEmailConfirm').val()) {
					if (true) {
						$.ajax({
							cache: false,
							data: {
								theEmail: $('#inputEmail').val(),
								theCity: '<?php echo $city; ?>',
								theState: '<?php echo $state; ?>',
								theZIP: '<?php echo $zip; ?>',
								theSIC: <?php echo (is_null($sic)) ? "''" : (int) $sic; ?>
							},
							dataType: 'html',
							success: function(response) {
								// this will throw an error in IE < 9, so do browser detection
								if (!($.browser.msie && $.browser.version < 9)) {
									console.log(response);
								}
							},
							url: 'scripts/sign_up.php',
							error: function(response) {
								// this will throw an error in IE < 9, so do browser detection
								if (!($.browser.msie && $.browser.version < 9)) {
									console.log(response);
								}
							}
						});
						$('#inputEmail_container').addClass('success');
						$('#inputEmailConfirmation_container').addClass('success');
						$('#inputEmailConfirmation_container span.success').css('display','block');
					}
				}
				else {
					$('#inputEmailConfirmation_container').addClass('error');
					$('#inputEmailConfirmation_container span.error').css('display','block');
				}
			}
			else {
				$('#inputEmail_container').addClass('error');
				$('#inputEmail_container span.error').css('display','block');
			}
		});

		function IsEmail(email) {
			var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regea.test(email);
		}
	});

	$(document).ready(function(){
	$("#Change").html("Change");
	});
</script>

<div class="row-fluid">
	<div class="span12">
		<div id="summary_table_holder">
			<table class="footable summary_table"  style="line-height:18px">
				<thead>
					<tr>
						<th><?php echo $regionLabel; ?><br/><?php echo $facilitiesCount . (($facilitiesCount == 1) ? ' Facility' : ' Facilities'); ?></th>
						<th class="phone-last">Change Since Last Month</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="expand">New Inspections</td>
						<td class="phone-last <?php echo ($new_inspections > 0) ? 'good_summary_cell' : ''; ?>">
							<div class="<?php echo ($new_inspections > 0) ? 'summary_good_val_holder' : 'summary_gray_val_holder'; ?>">
								<?php echo $new_inspections; ?>
							</div>
						</td>
					</tr>
					<tr>
					<tr>
						<td class="expand">New Violations</td>
						<td class="phone-last <?php echo ($new_violations > 0) ? 'bad_summary_cell' : ''; ?>">
							<div class="<?php echo ($new_violations > 0) ? 'summary_bad_val_holder' : 'summary_gray_val_holder'; ?>">
								<?php echo $new_violations; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td class="expand"><!--Changes In Status To "No Violation"-->Violations Resolved</td>
						<td class="phone-last <?php echo ($violations_resolved > 0) ? 'good_summary_cell' : ''; ?>">
							<div class="<?php echo ($violations_resolved > 0) ? 'summary_good_val_holder' : 'summary_gray_val_holder'; ?>">
								<?php echo $violations_resolved; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td class="expand">New Enforcement Actions</td>
						<td class="phone-last <?php echo ($new_enforcement_actions > 0) ? 'bad_summary_cell' : ''; ?>">
							<div class="<?php echo ($new_enforcement_actions > 0) ? 'summary_bad_val_holder' : 'summary_gray_val_holder'; ?>">
								<?php echo $new_enforcement_actions; ?>
							</div>
						</td>
					</tr>
					<tr style="background-color:white;font-weight:bold">
						<td style="text-align:center"><!--<?php echo $regionLabel; ?>--> Environmental Performance</td>
						<td class="<?php echo $summaryScoreClass; ?>" style="text-align:center"><?php echo $summaryScoreLabel; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/>

<?php
$count = count($results);
if ($count > 0) {
	if (!$all) {
?>
		<div class="truncated-results-header">
			Facilities which have had a change in the last month (<?php echo $count; ?> out of <?php echo $facilitiesCount; ?>)
		</div>
<?php
	}
?>
		<div id="result_table_holder">
			<table class="footable"  style="line-height:18px;">
				<thead>
					<tr>
						<th>Facility Name</th>
						<th data-hide="default,phone,tablet,desktop">Address</th>
						<th data-hide="default,phone,tablet,desktop">What They Do</th>
						<th data-hide="phone">Current Compliance</th>
						<th data-hide="phone">Enforcement Actions (5yrs)</th>
						<th data-hide="phone">Last Inspected</th>
						<th data-hide="default,phone,tablet,desktop">More Information</th>
						<th data-hide="default,tablet,desktop" style="display:none"><span class="dot_summary_title" id="Change"></span></th>
					</tr>
				</thead>
				<tbody>
<?php
	foreach ($results as $row) {
		$complianceClass = ($row['violation_resolved'] == 1) ? 'good_cell' : (($row['new_violation'] == 1) ? 'bad_cell' : '');
		$enforcementClass = ($row['new_enforcement_action'] == 1) ? 'bad_cell' : '';
		$inspectionClass = ($row['new_inspection'] == 1) ? 'good_cell' : '';
		$lastInspected = (is_null($row['Days_since_last_inspection'])) ? 'Never' : (($row['Days_since_last_inspection'] < 365) ? 'Within 1 year' : '> 1 year');
?>
					<tr>
						<td class="expand"><?php echo $row['Name']; ?></td>
						<td>
							<span style="display:inline-block;vertical-align:top" class="address" >
								<?php echo $row['Address']; ?><br/>
								<?php echo "{$row['City']}, {$row['State']} {$row['ZIP']}"; ?>
							</span>
						</td>
						<td>
							<span style="display:inline-block;vertical-align:top" class="SIC_labels" >
								<?php echo $row['SIC_Labels']; ?>
							</span>
						</td>
						<td><span class="<?php echo $complianceClass; ?>"><?php echo $row['Current_compliance_status']; ?></span></td>
						<td><span class="<?php echo $enforcementClass; ?>"><?php echo $row['Formal_Enf_Action_5yrs']; ?></span></td>
						<td><span class="<?php echo $inspectionClass; ?>"><?php echo $lastInspected; ?></span></td>
						<td><a href="<?php echo $row['Compliance_report']; ?>" target="_blank">Detailed Facility Report</a></td>
						<td>
							<div class="compliance_dots">
								<div class="dot <?php echo $complianceClass;?>_dot">&nbsp;</div>
								<div class="dot <?php echo $enforcementClass; ?>_dot">&nbsp;</div>
								<div class="dot <?php echo $inspectionClass; ?>_dot">&nbsp;</div>
							</div>
						</td>
					</tr>
<?php
	} // end foreach
?>
				</tbody>
			</table>
		</div>
<?php
} // end if count > 0
else if (!$all) {
?>
		<div class="truncated-results-header">
			Zero facilities in <?php echo $regionLabel; ?> had status changes in the past month.
		</div>
<?php
} // end else if
else {
?>
		<div class="truncated-results-header">
			There are zero facilities in <?php echo $regionLabel; ?>.
		</div>
<?php
}
// gracefully exit the database
$results = null;
$query = null;
$db = null;
?>
	</div>
<?php
if (!$sicSearch && !$all) {
?>
	<div>
		<a href="#" id="allFacSearch">Click here</a> to see all facilities in <?php echo $regionLabel; ?>.
		<script type="text/javascript">
			$('#allFacSearch').click(function(){
				$.ajax({
					cache: false,
					dataType: 'html',
					success: function (response) {
						$('#panel_content_holder').empty();
						$('#panel_content_holder').html(response);
					},
					url: 'ajax_content/results_table.php?<?php echo http_build_query($_GET) . '&all=true'; ?>'
				});
			});
		</script>
	</div>
<?php
} // end if !$sicSearch
?>

</div>