<?PHP
include 'scripts/getStateAbbreviation.php';
$tuCurl = curl_init();
if (isset($_GET['latitude'])) //call Mapquest API
{
	//Open Data API.  replace key with your own MapQuest Geocoding Key
	curl_setopt($tuCurl, CURLOPT_URL,
"http://open.mapquestapi.com/geocoding/v1/reverse?key=XXXXXXXXX&location=".$_GET['latitude'].",".$_GET['longitude']);


	curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
	$tuData = curl_exec($tuCurl);
	$output = json_decode($tuData, true); // decode json

	$city=$output['results'][0]['locations'][0]['adminArea5'];
	$state=$output['results'][0]['locations'][0]['adminArea3'];

	echo $city.", ".$state;
}

?>

