/**
*
* EnviroTRAK Version 1.0 (2012)
* Developed by Abt Associates (http://www.abtassociates.com/)
* All rights reserved.
*
*/

This application is available on the web at: http://envr.abtassociates.com/zw/

#####################################################
# About Abt Associates				    #
#####################################################

Abt Associates is arguably one of the most knowledgeable and experienced consultancies in terms of environmental programs, 
data flows, end uses, end users, and (EPA) enterprise and non-enterprise technology solutions. We believe that high value 
IT solutions and decision-making support must combine a) program expertise and understanding of end use(r)s with b) short 
cycles of discovery, scoping, design, prototyping, and development. When properly integrated into the development process, 
program and content expertise reduces development time and increases adoption.  How? As an example, creative and capable 
staff who bridge program support and application development can essentially crowd source requirements and end user needs 
on a daily basis. Even operating within the Agency's enterprise architecture, this married-to-content expertise development 
model has been key to some of EPA's more innovative and effective IT solutions supported by Abt Associates.  A selection of 
applications and IT solutions developed by Abt for other clients are presented at http://tech.abtassociates.com/engagements/ 

#####################################################
# Project Background                                #
#####################################################

Development of EnviroTRAK was funded entirely by Abt Associates.  The EnviroTRAK prototype was developed in 2012 to 
demonstrate a mobile device solution for individuals interested in monitoring adherence to environmental laws. We identified 
two end-user personas based on our support of EPA's ECHO website (http://echo.epa.gov) and its helpdesk. Jane Q. Public 
typically had intermittent interest in the topic and was most interested in poor compliance that might affect themselves, 
their family, or their community.  A secondary persona was from the regulated community and sought information with which to 
compare their facility's own performance. The source information is complex and detailed - anathemas to mobile solutions.  

The UX and design requirements included: simplified but accurate content and context to support the user personas' needs; 3-click 
navigation; responsive, sustainable device-agnostic platform; and the ability to sustain user engagement over time.  


#####################################################
# License                                           #
#####################################################

Please refer to license.txt within this repository to view details regarding proper use of this code and respective warranties 
and limitations. 


#####################################################
# Technologies Being Implemented                    #
#####################################################

EnviroTRAK was developed in 2012 using an agile development process and mobile-first responsive design; before either of these 
principles became standard practice at EPA.  While some of the technologies used to build this prototype are now dated, they were 
popular and cutting-edge when the prototype was being developed.

The tool is currently published on server running Windows Server 2008, but should work in a variety of environments including any
standard LAMP Stack.

Technologies and APIs being implemented in the EnviroTRAK prototype include:
	-PHP,
	-MYSQL,
	-jQuery,
	-AJAX,
	-Twitter Bootstrap (http://getbootstrap.com/),
	-Footable jQuery (http://fooplugins.com/plugins/footable-jquery/),	
	-MapQuest Geocoding Service (http://www.mapquestapi.com/geocoding/),
	-Amazon Simple Email Service (https://aws.amazon.com/ses/),	
	-Google Analytics (https://www.google.com/analytics/),
	-AddThis Sharing Buttons (https://www.addthis.com/get/sharing),
	-prettyLoader (http://www.no-margin-for-errors.com/projects/prettyloader/).	


For purposes of sharing this code, specific licenses and database passwords have been replaced with placeholder licenses/passwords.


#####################################################
# Installation/Deployment Instructions              #
#####################################################

To deploy this code in your own environment, the following steps should be taken:
	-The MYSQL database at "database/zombie_watch.sql" should be imported to your development environment.  The name of the db should 
	be zombie_watch.  Set your own username and password.
	-MYSQL Connection information should be updated in "ajax_content/results_table.php"
	-MapQuest Geocoding Service license information should be updated in "ajax_content/location_finder.php"
	-Google Analytics Tracking information should be updated in "index.html"
	-Amazon Simple Email Service Key should be updated in "scripts/sendUserFeedback.php"
	-Deploy all code to a web-accessible directory

#####################################################
# Data Source                                       #
#####################################################

The data used this prototype is current as of January 2013.


The original source of the data was EPA's ECHO Exporter Version 1.0.  Because the ECHO Exporter was not available via an API, Abt Associates 
developed a script to parse, format, and import the data to the desired MYSQL data structure.  EPA's Office of Compliance migrated to a new 
Oracle database (called the IDEA DataMart) in 2013, and has since published version 2.0 of the ECHO 
Exporter (http://echo.epa.gov/tools/data-downloads#exporter).  Some data fields in version 2.0 of the Exporter differ from version 1.0, however 
a similar script could easily be created to re-generate the SQL table using the new data source. 


#####################################################
# Future Development Plans                          #
#####################################################

Abt Associates is currently discussing features to be included in the development of EnviroTRAK Version 2.0.  Some of these features include:

	-Refreshing/updating the data to use ECHO Exporter version 2.0,
	-Incorporating a new "Sign Up/Keep Me Updated" feature that allows users to sign-up and receive automatic emails as conditions change,
	-Adding (508 compliant) sticky header/table plug-in for the data table,
	-Revising "i" information and caveats, 
	-Continuing user testing.